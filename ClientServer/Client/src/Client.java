import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

public class Client {

    public static void main(String[] args) throws IOException {
        try (Socket socket = new Socket("localhost", 8010);
             InputStream inputStream = socket.getInputStream();
             OutputStream outputStream = socket.getOutputStream()) {
            int response = 1;
            outputStream.write(response);
            System.out.println("Sent to server: " + response);

            while ((response = inputStream.read()) != -1) {
                System.out.println("Response server: " + response);
                if (response >= 10) {
                    break;
                }
                outputStream.write(response++);
                System.out.println("Sent to server: " + response);
                outputStream.flush();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
