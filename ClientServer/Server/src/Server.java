import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {

    public static void main(String[] args) throws IOException {
        ServerSocket serverSocket = new ServerSocket(8010);
        System.out.println("The server is waiting for the client...");

        try (Socket clientSocket = serverSocket.accept();
             InputStream inputStream = clientSocket.getInputStream();
             OutputStream outputStream = clientSocket.getOutputStream()) {

            System.out.println("New connection - " + clientSocket.getInetAddress().toString());
            int request;

            while ((request = inputStream.read()) != -1) {
                System.out.println("Client's request: " + request);
                outputStream.write(++request);
                System.out.println("Sent to client: " + request);
                outputStream.flush();
            }
            System.out.println("Client disconnected");
        }
    }
}
