import java.io.*;
import java.net.Socket;
import java.nio.charset.Charset;
import java.util.Scanner;

public class Client {

    public static void main(String[] args) throws IOException {
        try (Socket socket = new Socket("localhost", 8010);
             BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream(), Charset.forName("UTF-8")));
             BufferedWriter out = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream(), Charset.forName("UTF-8")))) {

            Scanner sc = new Scanner(System.in);

            String line;

            do {
                System.out.println("Line to server: ");
                line = sc.nextLine();
                out.write(line + "\r\n");
                out.flush();
                System.out.println("Server answer: " + in.readLine());

            } while (!line.equals("exit"));

            out.write("exit");

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
