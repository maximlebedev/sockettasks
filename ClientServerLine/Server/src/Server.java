import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.Charset;

public class Server {

    public static void main(String[] args) throws IOException {
        ServerSocket serverSocket = new ServerSocket(8010);
        System.out.println("The server is waiting for the client...");

        try (Socket clientSocket = serverSocket.accept();
             BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream(), Charset.forName("UTF-8")));
             BufferedWriter out = new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream(), Charset.forName("UTF-8")))) {

            System.out.println("New connection - " + clientSocket.getInetAddress().toString());

            String request = "";

            while (!request.equals("exit")) {
                request = in.readLine();
                out.write(request + "\r\n");
                out.flush();
            }

            System.out.println("Client disconnected");
        }
    }
}
